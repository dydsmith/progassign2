/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#ifndef POINT_H
#define POINT_H

/* Point class represents the coordinate of each pixel in the image */

class Point {
	private:
   	double x;
		double y;

	public:
		void setX(double);
		void setY(double);
		double getX();
		double getY();

};
#endif
