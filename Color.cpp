/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#include "Color.h"

/* One consolidated setter to set the RGB values of the pixel and 
	three getters, one for each color channel. */

void Color::setRGB(unsigned char r, unsigned char g, unsigned char b) {
	red = r;
	green = g;
	blue = b;
	return;
}

unsigned char Color::getRed() {
	return red;
}

unsigned char Color::getGreen() {
	return green;
}

unsigned char Color::getBlue() {
	return blue;
}

