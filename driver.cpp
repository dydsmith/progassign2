/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#include "Image.h"

// The client function checkPoint only takes in a Point vector
bool checkPoint(vector<Point> p);

int main(int argc, char *argv[]) {

	// Variables list, not arranged in any particular order
	ofstream outPut(argv[2]);
	ifstream inPut(argv[1]);
	Image outputImage;
	vector<Point> triangle(4);
	int i = 0, j = 0, w = 0, h = 1, maxValue = 255;
	double x = 0.0, y = 0.0;
	string m = "P6";
	bool hitPoint;

	if(argc != 3) {
		cout << "USAGE ERROR:  ./executable inPutFileName outPutFileName";
		return 1;
	}

	if(inPut.fail()) {
		cout << argv[1] << " did not open successfully\n";
		return 2;
	}

	if(outPut.fail()) {
		cout << argv[2] << " did not open successfully\n";
		return 3;
	}
    
	// Read in header info from the input file and initialize that data in
	// the instance of the Image class
	inPut >> w >> h;
	outputImage.setHeader(m, w, h, maxValue);
	outputImage.printHeader(outPut);

	// Use a for-loop to get the points of the triangle from the input file
	// and put them into the Point vector (triangle)
	for(; i < 3; i++) {
		inPut >> x >> y;

		// Handle triangle points that are outside the image's plane
		if(x > w || y > h) {
			cout << "ERROR: Triangle point number " << i << " is outside the ";
			cout << "range of the image." << endl;
			return 4;
		}

		triangle.at(i).setX(x);
		triangle.at(i).setY(y);
	}

	// Set up the coordinate system in the image
	outputImage.setPoints();

	/* Use a nested for-loop to
	 * 1) create a test point in the last element of our Point vector (triangle)
	 * 2) check to see if that test point lies within the triangle
	 * 3) assign a color value to the pixel associated with that point based
	 		on whether or not the point lies within the triangle
	 */
	for(i = 0; i < outputImage.getHeight(); i++) {
		for(j = 0; j < outputImage.getWidth(); j++) {
			triangle.at(3).setX(outputImage.getX(i, j));
			triangle.at(3).setY(outputImage.getY(i, j));

			hitPoint = checkPoint(triangle);

			if(hitPoint) {
				outputImage.setColor(100, 170, 30, i, j);
			}
			else {
				outputImage.setColor(80, 20, 140, i, j);
			}
		}
	}
	
	// Print the pixels in the image
	outputImage.printColor(outPut);

	// Remember to free up dynamically allocated memory
	inPut.close();
	outPut.close();

	return 0;
}

// checkPoint tests a point to see if it lies within a triangle defined by
// three points in a plane
bool checkPoint(vector<Point> p) {
	double a, b, c;
	int conditionsMet = 0;

	// Use the Barycentric coordinate system to test the points
	a = (	( p.at(1).getY() - p.at(2).getY() ) *
			( p.at(3).getX() - p.at(2).getX() ) +
			( p.at(2).getX() - p.at(1).getX() ) *
			( p.at(3).getY() - p.at(2).getY() )		)
								  /
		 (	( p.at(1).getY() - p.at(2).getY() ) *
			( p.at(0).getX() - p.at(2).getX() ) +
			( p.at(2).getX() - p.at(1).getX() ) *
			( p.at(0).getY() - p.at(2).getY() ) 	);

	b = ( ( p.at(2).getY() - p.at(0).getY() ) *
			( p.at(3).getX() - p.at(2).getX() ) +
			( p.at(0).getX() - p.at(2).getX() ) *
			( p.at(3).getY() - p.at(2).getY() ) 	)
		 						  /
		 ( ( p.at(1).getY() - p.at(2).getY() ) *
		 	( p.at(0).getX() - p.at(2).getX() ) +
			( p.at(2).getX() - p.at(1).getX() ) *
			( p.at(0).getY() - p.at(2).getY() ) 	);

	c = 1 - a - b;

	if(a >= 0 && a <= 1) {
		conditionsMet++;
	}
	if(b >= 0 && b <= 1) {
		conditionsMet++;
	}
	if(c >= 0 && c <= 1) {
		conditionsMet++;
	}

	if(conditionsMet == 3) {
		return true;
	}
	else {
		return false;
	}

}

