/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#ifndef PIXEL_H
#define PIXEL_H
#include "Color.h"
#include "Point.h"
using namespace std;

/* The pixel class represents each pixel in the image. Each pixel has a
	color and a coordinate, so the Pixel class contains both a Color and a
	Point object. */

class Pixel {
	private:
   	Color color;
		Point coord;
	
	public:
		void setColor(unsigned char, unsigned char, unsigned char);
		void setCoords(double, double);
		double getXCoord();
		double getYCoord();
		unsigned char getR();
		unsigned char getG();
		unsigned char getB();

};
#endif
