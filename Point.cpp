/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#include "Point.h"

/* Two setters and getters for the x and y coordinates of each pixel. */

void Point::setX(double xCoord) {
	x = xCoord;
	return;
}

void Point::setY(double yCoord) {
	y = yCoord;
	return;
}

double Point::getX() {
	return x;
}

double Point::getY() {
	return y;
}

