/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#ifndef COLOR_H
#define COLOR_H

/* The Color class assigns the RGB color values to each Pixel
	in the image */

class Color {
	private:
		unsigned char red;
		unsigned char green;
		unsigned char blue;

	public:
		void setRGB(unsigned char, unsigned char, unsigned char);
		unsigned char getRed();
		unsigned char getGreen();
		unsigned char getBlue();
    
};

#endif
