/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#ifndef IMAGE_H
#define IMAGE_H
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "Pixel.h"
#include "Header.h"
using namespace std;

/* Image class represents all of the data within the image. It contains a
	header class instance to hold the header information and a vector of
	Pixel objects to hold the data (color and coordinate) for all of the
	pixels in the image. It also contains member functions to print the
	header to the output image and to print the pixels to the output image. */

class Image {
	private:
		vector<Pixel> pixels;
		Header imageHdr;

	public:
		void setHeader(string, int, int, int);
		void printHeader(ofstream&);
		int getWidth();
		int getHeight();
		void setPoints();
		double getX(int, int);
		double getY(int, int);
		void setColor(unsigned char, unsigned char, unsigned char, int, int);
		void printColor(ofstream&);

};
#endif
