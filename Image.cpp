/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#include "Image.h"

// setHeader initializes the header instance inside the Image class
void Image::setHeader(string m, int w, int h, int max) {
	imageHdr.setAll(m, w, h, max);
	pixels.resize(w * h); // Must resize the pixel vector according to the
								 // image's width and height
	return;
}


// printHeader prints the image's header info to the output image
void Image::printHeader(ofstream& outFile) {
	outFile << imageHdr.getMagic() << endl;
	outFile << imageHdr.getWidth() << " " << imageHdr.getHeight() << " ";
	outFile << imageHdr.getMaxVal() << endl;
	return;
}


// getWidth and getHeight functions are required in the driver program
int Image::getWidth() {
	return imageHdr.getWidth();
}

int Image::getHeight() {
	return imageHdr.getHeight();
}


// setColor initializes the coordinates for all of the pixels in the image
void Image::setPoints() {
	int i = 0, j = 0;

	/* Use a nested for-loop to initialize each pixel in the pixels vector.
	 * Nested for-loops are used in member functions whenever the pixels
	 * vector needs to be accessed */
	for(; i < imageHdr.getHeight(); i++) {
		for(j = 0; j < imageHdr.getWidth(); j++) {
			pixels.at(imageHdr.getWidth() * i + j).setCoords(j, i);
		}
	}

	return;
}


/* getX and getY are also required in the driver program.
 * The i and j parameters are due to these functions being used in a nested
 * for-loop in the driver program */
double Image::getX(int i, int j) {
	return pixels.at(imageHdr.getWidth() * i + j).getXCoord();
}

double Image::getY(int i, int j) {
	return pixels.at(imageHdr.getWidth() * i + j).getYCoord();
}


/* setColor assigns the RGB values to the appropriate pixel based on
 * the current iteration in a for-loop in the driver program (hence the
 * i and j parameters) */
void Image::setColor(unsigned char r, unsigned char g, unsigned char b,
		int i, int j) {
	pixels.at(imageHdr.getWidth() * i + j).setColor(r, g, b);
	return;
}


// printColor prints the RGB values for each pixel into the output image
void Image::printColor(ofstream& outFile) {
	int i = 0, j = 0;

	for(; i < imageHdr.getHeight(); i++) {
		for(j = 0; j < imageHdr.getWidth(); j++) {
			outFile << pixels.at(imageHdr.getWidth() * i + j).getR();
			outFile << pixels.at(imageHdr.getWidth() * i + j).getG();
			outFile << pixels.at(imageHdr.getWidth() * i + j).getB();
		}
	}

	return;
}

