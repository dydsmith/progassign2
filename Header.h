/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#ifndef HEADER_H
#define HEADER_H
#include <string>
using namespace std;

/* Header class represents the header to the ppm image file. */

class Header {
	private:
		string magic;
		int width;
		int height;
		int maxVal;

	public:
		void setAll(string, int, int, int);
		string getMagic();
		int getWidth();
		int getHeight();
		int getMaxVal();

};

#endif
