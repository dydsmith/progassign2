/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#include "Header.h"

/* One setter to set all of the member variables of the Header class. Four
	getters, one for each member variable. */

void Header::setAll(string m, int w, int h, int max) {
	magic = m;
	width = w;
	height = h;
	maxVal = max;
	return;
}

string Header::getMagic() {
	return magic;
}

int Header::getWidth() {
	return width;
}

int Header::getHeight() {
	return height;
}

int Header::getMaxVal() {
	return maxVal;
}

