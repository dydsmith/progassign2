/********************
*Daniel N Smith
*CPSC 1020-002, Sp17
*dnsmith
********************/

#include "Pixel.h"

/* Two setters, one for the color class and one for the point class.
	Five getters, two for the point class and three for the color class. */

void Pixel::setColor(unsigned char r, unsigned char g, unsigned char b) {
	color.setRGB(r, g, b);
	return;
}

void Pixel::setCoords(double x, double y) {
	coord.setX(x);
	coord.setY(y);
	return;
}

double Pixel::getXCoord() {
	return coord.getX();
}

double Pixel::getYCoord() {
	return coord.getY();
}

unsigned char Pixel::getR() {
	return color.getRed();
}

unsigned char Pixel::getG() {
	return color.getGreen();
}

unsigned char Pixel::getB() {
	return color.getBlue();
}

